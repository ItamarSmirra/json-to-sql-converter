const fs = require('fs');
const columnsNames = new Map();
const readLineSync = require('readline-sync');
const fileName = readLineSync.question("Enter the json file name without the preffix - XXX.json\nThe table name will be - t_XXX\n File name -> ");
const tableName = `t_${fileName}`;

const DMLTemplate = `INSERT INTO ${tableName} (`;
const DMLArray = [];
let DDL = "DROP TABLE " + tableName + ";\nCREATE TABLE " + tableName + " (\n";

// If key already exist in map
const containsKey = (map, wantedKey) => {
    let doesExist = false;

    map.forEach((value, key) => {
        if (key == wantedKey) {
            doesExist = true;
        }
    });

    return doesExist;
};

const typeToSqlType = (type) => {
    if (isNaN(type)) {
        return 'VARCHAR2(256)'
    } else {
        return 'NUMBER'
    }
}

// Code Section
let rawData = fs.readFileSync(`${fileName}.json`);
let jsonArray = JSON.parse(rawData);

// Running through all the json elements
jsonArray.forEach(json => {
    const currColumns = [];
    const currValues = [];

    // Running through the json keys
    Object.keys(json).forEach(key => {
        if(!containsKey(columnsNames, key)) {
            columnsNames.set(key, json[key]);
        }

        currColumns.push(key);
        let currVal = json[key];
		if (currVal == null) {
			currVal = "NULL";
		} else if (isNaN(currVal)) {
            currVal = "'" + json[key].split("'").join("`") + "'";
        }
		
        currValues.push(currVal);
    });

    DMLArray.push(DMLTemplate + currColumns.join(", ") + ') VALUES(' + currValues.join(", ") + ');');
});

columnsNames.forEach((type, column) => {
    let pk = '';

    if (column == 'id') {
        pk = ' Primary Key';
    }

    DDL += column + ' ' + typeToSqlType(type) + pk + ',\n';
})

DDL = DDL.substring(0, DDL.length - 2) + "\n);";
fs.writeFileSync("DDL.sql", DDL);
fs.writeFileSync("DML.sql", DMLArray.join('\n') + '\nCOMMIT;');
console.log("Compilted!");